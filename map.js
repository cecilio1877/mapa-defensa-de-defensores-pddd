let map = L.map('map').setView([-2.86,-57.66], 5.4)

// crear mapa base
L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.png',{
    attribution: '© <a href="https://osm.org/copyright">OpenStreetMap<\/a> contributors'
}).addTo(map)



//cargar archivo geojson
const archivo = async ()=>{

let capa = document.querySelectorAll('.leaflet-interactive')




    //crear popup
const popup = (feature, layer)=>{


        layer.bindPopup("<div class='titulo'><b>"+feature.properties.nombre+"</b></div>"+
        "</br><img class='imagen' src="+feature.properties.image+"></br></br><div class='contenido'>"+feature.properties.description+"</div>")

}

function stylePolygon(feature) {
    return {
      weight: 1.3, // grosor de línea
      color: 'yellow', // color de línea
      opacity: 1.0, // tansparencia de línea
      fillColor: 'green', // color de relleno
      fillOpacity: 0.7, // transparencia de relleno
      stroke: "#AA801B"
    };
   };




try {
    const res = await fetch('todo.geojson')
  const puntos = await res.json()



//agregar los puntos
    L.geoJSON(puntos).addTo(map)








       //pintar el popup
/* let info = L.geoJSON(puntos,{
        onEachFeature: popup,
        style: stylePolygon,



    }).addTo(map) */

function onEachFeature(feature, layer) {
        //bind click
        layer.on('click', function (e) {
          //popup
          layer.setStyle({fillColor:'#006633'})

          map.eachLayer(function( layer){
            layer.bindPopup("<div class='titulo'><b>"+feature.properties.titulo+"</b></div>"+
        //"</br><img class='imagen' src="+feature.properties.description+"></br>"+
        "</br><div class='nombre'>"+feature.properties.nombre1+"</div>"+
        "<div class='cargo'>"+feature.properties.cargo1+"</div>"+
        "<center><img class='imagen1' src="+feature.properties.foto1+"></img></center></br>"+
        "</br><div class='nombre'>"+feature.properties.nombre2+"</div>"+
        "<div class='cargo'>"+feature.properties.cargo2+"</div>"+
        "<center><img class='imagen1' src="+feature.properties.foto2+"></img></center></br>"+
        "<div class='nombre'>"+feature.properties.nombre3+"</div>"+
        "<div class='cargo'>"+feature.properties.cargo3+"</div>"+
        "<center><img class='imagen1' src="+feature.properties.foto3+"></img></center>")

          })

        /* let cerrar = document.getElementsByClassName('.leaflet-popup-close-button')
        cerrar.addEventListener('click',(e)=>{
            e.setStyle({fillColor:'green'})
        }) */



        });
        layer.setStyle({fillColor:'green'})

    }


let info = L.geoJSON(puntos,{
        style: stylePolygon,
        onEachFeature: onEachFeature,
        //zoomControl:false


    }).addTo(map)




    map.scrollWheelZoom.disable();




} catch (error) {
    console.log(error)

    }

}

archivo()
